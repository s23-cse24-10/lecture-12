#ifndef SEARCH_H
#define SEARCH_H 

#include <vector>

bool binarySearch(std::vector<int> nums, int x) {
    int l = 0;
    int r = nums.size() - 1;

    while (l <= r) {
        int mid = (l + r) / 2;

        if (nums[mid] == x) {
            return true;
        }

        if (nums[mid] < x) {
            l = mid + 1;
        } else {
            r = mid - 1;
        }
    }

    return false;
}




#endif 
#include <iostream>
#include <vector>
#include "search.h"
using namespace std;

int main(int argc, char* argv[]) {

    vector<int> nums = {1,2,3,4,5};

    bool result = binarySearch(nums, 5);

    if (result) {
        cout << "WE FOUND IT" << endl;
    } else {
        cout << "NOT FOUND" << endl;
    }

	return 0;
}

#include <igloo/igloo.h>
#include <unistd.h>
#include "search.h"

using namespace igloo;

Context(TestAll){
    int oldSTDOUT;
    int oldSTDERR;

    void SetUp() {
        oldSTDOUT = dup( 1 );
        oldSTDERR = dup( 2 );
        freopen( "/dev/null", "w", stdout );
        freopen( "/dev/null", "w", stderr );     
    }

    void TearDown() {
        fflush( stdout );
        fflush( stderr );
        dup2( oldSTDOUT, 1 );
        dup2( oldSTDERR, 2 );
    }

    // Your specs here

    Spec(MiddleElementFound){
        std::vector<int> numbers = {1,2,3,4,5};
        Assert::That(binarySearch(numbers, 3), IsTrue());
    }

    Spec(FirstElementFound){
        std::vector<int> numbers = {1,2,3,4,5};
        Assert::That(binarySearch(numbers, 1), IsTrue());
    }

    Spec(LastElementFound){
        std::vector<int> numbers = {1,2,3,4,5};
        Assert::That(binarySearch(numbers, 5), IsTrue());
    }

    Spec(SmallerElementNotFound){
        std::vector<int> numbers = {1,2,3,4,5};
        Assert::That(binarySearch(numbers, 0), IsFalse());
    }

    Spec(LargerElementNotFound){
        std::vector<int> numbers = {1,2,3,4,5};
        Assert::That(binarySearch(numbers, 42), IsFalse());
    }
};

int main(int argc, const char* argv[]){
    return TestRunner::RunAllTests(argc, argv);
}